#include <sys/mman.h>

#include "mem_internals.h"
#include "mem.h"
#include "test.h"
#include "util.h"

#define HEAP_SIZE 1024
#define BLOCK_SIZE 128

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void test1(void) {
    fprintf(stdout, "Test №1 (Обычное успешное выделение памяти)\n");

    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    if (!heap) {
        fprintf(stderr, "Fail with heap\n");

        return;
    }

    void* mem = _malloc(BLOCK_SIZE);
    debug_heap(stdout, heap);

    if (!mem) {
        fprintf(stderr, "Fail with malloc\n");

        return;
    }

    free_heap(heap, HEAP_SIZE);
    fprintf(stdout, "Test №1 - Sucessful passed\n");
}

void test2(void) {
    fprintf(stdout, "Test №2 (Освобождение одного блока из нескольких выделенных)\n");

    void* heap = heap_init(HEAP_SIZE);
    void* mem1 = _malloc(BLOCK_SIZE);
    void* mem2 = _malloc(2 * BLOCK_SIZE);  
    debug_heap(stdout, heap); 

    _free(mem2);
    fprintf(stdout, "Start freeing\n");
    debug_heap(stdout, heap);

    _free(mem1);
    free_heap(heap, HEAP_SIZE);
    fprintf(stdout, "Test №2 - Sucessful passed\n");
}

void test3(void) {
    fprintf(stdout, "Test №3 (Освобождение двух блоков из нескольких выделенных)\n");

    void* heap = heap_init(2 * HEAP_SIZE);
    void* mem1 = _malloc(BLOCK_SIZE);
    void* mem2 = _malloc(2 * BLOCK_SIZE);
    void* mem3 = _malloc(8 * BLOCK_SIZE);
    debug_heap(stdout, heap); 

    _free(mem2);
    fprintf(stdout, "Start freeing 1\n");
    debug_heap(stdout, heap);

    _free(mem1);
    fprintf(stdout, "Start freeing 2\n");
    debug_heap(stdout, heap);

    _free(mem3);
    free_heap(heap, 2 * HEAP_SIZE);
    fprintf(stdout, "Test №3 - Sucessful passed\n");
}

void test4(void) {
    fprintf(stdout, "Test №4 (Память закончилась, новый регион памяти расширяет старый)\n");

    void* heap = heap_init(HEAP_SIZE / 8);
    debug_heap(stdout, heap);
    void* mem1 = _malloc(BLOCK_SIZE * BLOCK_SIZE);
    debug_heap(stdout, heap);

    _free(mem1);
    free_heap(heap, 16 * HEAP_SIZE);
    fprintf(stdout, "Test №4 - Sucessful passed\n");
}

void test5(void) {
    fprintf(stdout, "Test №5 (Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.)\n");

    void* heap = heap_init(HEAP_SIZE / 8);
    debug_heap(stdout, heap);
    void* mem1 = _malloc(BLOCK_SIZE * BLOCK_SIZE);
    debug_heap(stdout, heap);

    struct block_header* header = block_get_header(mem1);

    (void) mmap(header->contents + header->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);


    void* mem2 = _malloc(BLOCK_SIZE * BLOCK_SIZE);
    printf("New heap\n");      
    debug_heap(stdout, heap);                     


    if (!mem2) {
        fprintf(stderr, "Fail with malloc\n");
        
        return;
    }

    _free(mem1);
    _free(mem2);
    free_heap(heap, 32 * HEAP_SIZE);
    fprintf(stdout, "Test №5 - Sucessful passed\n");
}
